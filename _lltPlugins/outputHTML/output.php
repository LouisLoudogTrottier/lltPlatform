<!DOCTYPE html><!-- <?=basename(__FILE__).' '.filemtime(__FILE__)?> -->
<html>
    <head>
	<?=call_user_func([$LLTP,'getContent'],'heads'); ?>
	
	<?=call_user_func([$LLTP,'getContent'],'styles'); ?>
	
	<?=call_user_func([$LLTP,'getContent'],'scripts'); ?>
    </head>
    <body>
	
	<header><?=call_user_func([$LLTP,'getContent'],'header'); ?></header>		
	
	<nav><?=call_user_func([$LLTP,'getContent'],'navmenu'); ?></nav>
	
	<div name="errors"><?=call_user_func([$LLTP,'getContent'],'errors'); ?></div>
	
	<section><?=call_user_func([$LLTP,'getContent'],'section'); ?></section>		
		
	<footer><?=call_user_func([$LLTP,'getContent'],'footer'); ?></footer>		
	
	<?=call_user_func([$LLTP,'getContent'],'debugs'); ?>
	
    </body>
</html>