<?php
## GET #######################################################################################################
if(!($_STYLES=call_user_func([$LLTP,'getNode'],'styles'))): unset($_STYLES); return; endif;

## RUN #######################################################################################################
foreach ($_STYLES as $_STYLE => $_GLOBS):
    @call_user_func([$LLTP,'debugStamp'],'[SCRIPTS]['.$_STYLE.'] '.json_encode($_GLOBS));
    ob_start();
	
    foreach(call_user_func([$LLTP,'asGlobs'],$_GLOBS)?:[] as $_CSS):
		?><link name="<?=$_STYLE?>" type="text/css" rel="stylesheet" href="<?= str_replace(LLTP_ROOT,'',$_CSS);?>" /><?php
		unset($_CSS); endforeach;
	
    call_user_func([$LLTP,'addContent'],'styles', ob_get_contents());
    ob_end_clean();
	unset($_STYLES,$_GLOBS);
endforeach;

## END #######################################################################################################
unset($_STYLES);
return;

