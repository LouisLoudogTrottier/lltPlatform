<?php
return [
    'contentHeader'=>[
        'type'=>['','php','html','app'],
        'contents'=>[ 'header'=>__DIR__.'/header.php', ],
        'scripts'=>[ 'layoutHeader'=>__DIR__.'/header.js', ],
        'styles'=>[ 'layoutHeader'=>__DIR__.'/header.css', ],
    ],
];