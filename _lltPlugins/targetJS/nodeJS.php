
<?php
## GET #######################################################################################################
if(!($_SCRIPTS=call_user_func([$LLTP,'getNode'],'scripts'))): unset($_SCRIPTS); return; endif;

## RUN #######################################################################################################
foreach ($_SCRIPTS as $_SCRIPT => $_GLOBS):
    @call_user_func([$LLTP,'debugStamp'],'[SCRIPTS]['.$_SCRIPT.'] '.json_encode($_GLOBS));
    ob_start();
	
    foreach(call_user_func([$LLTP,'asGlobs'],$_GLOBS)?:[] as $_JS):
		?><script name="<?=$_SCRIPT?>" type="text/javascript"  src="<?= str_replace(LLTP_ROOT,'',$_JS);?>"></script><?php
		unset($_JS); endforeach;
	
    call_user_func([$LLTP,'addContent'],'scripts', ob_get_contents());
    ob_end_clean();
	unset($_SCRIPTS,$_GLOBS);
endforeach;

## END #######################################################################################################
unset($_SCRIPTS);
return;
