<?php
// AJAX ########################################################################################

return[
    'json'=>[
	'type'=>'json',
	'headers'=>[
	    'Content-Type:text/json',
	    #'Access-Control-Allow-Credentials:true',
	],
	'workloads'=>[
	    'content'=>false,
	    'output'=>__DIR__.'/output.php',
	],
    ],
];   