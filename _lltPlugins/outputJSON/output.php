<?php
/** ##############################################################################################
 *
 */
header('Content-type:text/json',true);
@header('Access-Control-Allow-Origin: '.(trim($_SERVER['HTTP_REFERER'],'/')?:'null'),true);
header('Access-Control-Allow-Credentials:true',true);

echo json_encode(call_user_func([$LLTP,'getDatas'])?:[]);
