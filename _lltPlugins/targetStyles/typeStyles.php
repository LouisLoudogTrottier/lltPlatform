<?php
## GET #######################################################################################################
if(!($_STYLES=call_user_func([$LLTP,'getType'],'styles'))): unset($_STYLES); return; endif;

## RUN #######################################################################################################
foreach ($_STYLES as $_STYLE => $_GLOBS):
    @call_user_func([$LLTP,'debugStamp'],'[STYLES]['.$_STYLE.'] '.json_encode($_GLOBS));
    ob_start();
	
    foreach(call_user_func([$LLTP,'asGlobs'],$_GLOBS)?:[] as $_CSS):
		?><style name="<?=$_STYLE?>" type="text/css"><?php include $_CSS ?></style><?php
		unset($_CSS); endforeach;
	
    call_user_func([$LLTP,'addContent'],'styles', ob_get_contents());
    ob_end_clean();
	unset($_STYLES,$_GLOBS);
endforeach;

## END #######################################################################################################
unset($_STYLES);
return;
