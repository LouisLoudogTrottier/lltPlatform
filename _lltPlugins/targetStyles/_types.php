<?php
// AJAX ########################################################################################
return[
    'targetStyles'=>[
	'type'=>['html','app'],
	'workloads'=>[ 
	    'target'=>[
		'styles'=>[
		    'nodes'=>[__DIR__.'/nodeStyles.php'],
		    'types'=>[__DIR__.'/typeStyles.php'],
		]
	    ],
	    'output'=>[],
	],
    ],
]; 