<?php
return [
    'contentFooter'=>[
        'type'=>['','php','html','app'],
        'contents'=>[ 'footer'=>__DIR__.'/footer.php', ],
        'scripts'=>[ 'layoutFooter'=>__DIR__.'/footer.js', ],
        'styles'=>[ 'layoutFooter'=>__DIR__.'/footer.css', ],
    ],
];