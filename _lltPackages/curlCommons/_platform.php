<?php

trait curlCommons {
    private $curlCommons=[];
    public function getCurl(string $k='exec'){ return $this->curlCommons[$k]; }
    public function setCurl(array $curl,bool $replace=true){
	$this->curlCommons=$replace
	    ? array_replace_recursive($this->curlCommons,$curl)
	    : array_merge_recursive($this->curlCommons,$curl);
    }
    public function runCurl(){
	if(empty($this->curlCommons['url'])) return error_log(__FUNCTION__.' Empty url');
	#print_r($this->curlCommons);
	try{
	    $this->runCurlInit();
	    $this->runCurlUrl();
	    $this->runCurlUserAgent();
	    $this->runCurlHeader();
	    $this->runCurlReferer();
	    $this->runCurlEncoding();
	    $this->runCurlCookieFile();
	    $this->runCurlCookieJar();
	    $this->runCurlFreshConnect();
	    $this->runCurlReturnTransfer();
	    $this->runCurlTimeout();
	    $this->runCurlFollowLocation();
	    $this->runCurlExec();
		
	} catch (Exception $ex) { error_log($ex); }
		$this->runCurlClose();
		#print_r($this->curlCommons);
    }
    private function runCurlInit(){ 
		$this->curlCommons['curl']=curl_init();
    }
    private function runCurlUrl(){ 
		if(empty($this->curlCommons['curl'])) return false;
		curl_setopt($this->curlCommons['curl'], CURLOPT_URL, $this->curlCommons['url']);
    }
    private function runCurlUserAgent(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['useragent'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_USERAGENT, $this->curlCommons['useragent']);
    }
    private function runCurlHeader(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['httpheader'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_HTTPHEADER, $this->curlCommons['httpheader']);
    }
    private function runCurlReferer(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['referer'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_REFERER, $this->curlCommons['referer']);
    }
    private function runCurlEncoding(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['encoding'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_ENCODING, $this->curlCommons['encoding']);
    }
    private function runCurlCookieFile(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['cookiefile'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_COOKIEFILE, $this->curlCommons['cookiefile']);
    }
    private function runCurlCookieJar(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['cookiejar'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_COOKIEJAR, $this->curlCommons['cookiejar']);
    }
    private function runCurlFreshConnect(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['freshconnect'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_FRESH_CONNECT, $this->curlCommons['freshconnect']);
    }
    private function runCurlReturnTransfer(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['returntransfer'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_RETURNTRANSFER, $this->curlCommons['returntransfer']);
    }
    private function runCurlTimeout(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['timeout'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_TIMEOUT, $this->curlCommons['timeout']);
    }
    private function runCurlFollowLocation(){ 
		if(empty($this->curlCommons['curl'])) return false;
		if(empty($this->curlCommons['followlocation'])) return true;
		return curl_setopt($this->curlCommons['curl'], CURLOPT_FOLLOWLOCATION, $this->curlCommons['followlocation']);
    }
    private function runCurlExec(){
		if(empty($this->curlCommons['curl'])) return false;
		ob_start();
		curl_exec($this->curlCommons['curl']);
		$this->curlCommons['exec']= ob_get_contents();
		ob_end_clean();
		return ($this->curlCommons['exec']);
    }
    private function runCurlClose(){
		return curl_close($this->curlCommons['curl']);
    }
   
}
 