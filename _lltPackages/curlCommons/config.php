<?php

return[
    'httpheader'=>[
		"Accept: text/xml,application/xml,application/xhtml+xml,"
			. "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
		"Cache-Control: max-age=0",
		//"Connection: keep-alive",
		//"Keep-Alive: 300",
		"Accept-Charset: utf-8,ISO-8859-1;q=0.7,*;q=0.5",
		"Accept-Language: ".filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'),
		"REMOTE_ADDR: ".filter_input(INPUT_SERVER,'REMOTE_ADDR'),
		"X_FORWARDED_FOR: ".filter_input(INPUT_SERVER,'REMOTE_ADDR'),
		"Pragma:",
    ],
];