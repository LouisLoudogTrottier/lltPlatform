<?php

/** CURL #########################################################################################
 * 
 */
call_user_func([$LLTP,'setCurlCommons'],['url'=>'http://127.0.0.1']);
call_user_func([$LLTP,'runCurlCommons']);
call_user_func([$LLTP,'debugPre'],call_user_func([$LLTP,'getCurlCommons']));


/** WITH HEADERS #################################################################################
 * 
 */
call_user_func([$LLTP,'setCurlCommons'],[
    'url'=>'example.com',
    'httpheader'=>[
		"Accept: text/xml,application/xml,application/xhtml+xml,"
	    . "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
		"Cache-Control: max-age=0",
		//"Connection: keep-alive",
		//"Keep-Alive: 300",
		"Accept-Charset: utf-8,ISO-8859-1;q=0.7,*;q=0.5",
		"Accept-Language: ".filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'),
		"REMOTE_ADDR: ".filter_input(INPUT_SERVER,'REMOTE_ADDR'),
		"X_FORWARDED_FOR: ".filter_input(INPUT_SERVER,'REMOTE_ADDR'),
		"Pragma:",
    ],
]);
call_user_func([$LLTP,'runCurlCommons']);
call_user_func([$LLTP,'debugPre'],call_user_func([$LLTP,'getCurlCommons']));
