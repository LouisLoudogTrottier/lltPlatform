<?php

trait frameTypes {

    private $frameType = [];
    private $frameTypes = [];
    
    private function __construct__frameTypes() {
	#@$this->includeTypes(__DIR__ . '/types.php');
		$this->__packages__frameTypes();
    }
    private function __packages__frameTypes() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_types.php") as $_INCLUDE):
	    $this->includeTypes($_INCLUDE,FALSE); unset($_INCLUDE); endforeach; 
	    unset($_PACKAGE); endforeach;
    }
    public function getType(string $_ = null) {
	$this->frameType['type']= array_unique(self::asArray($this->frameType['type']));
	return call_user_func([$this, 'arrayRecurse'], $this->frameType, func_get_args());
    }
    public function getTypes(string $_ = null) {
	return call_user_func([$this, 'arrayRecurse'], $this->frameTypes, func_get_args());
    }
    public function setType(array $_ARRAY = null, bool $_REPLACE = true) {
	$this->frameType = call_user_func([$this, 'arrayMerge'], $this->frameType, $_ARRAY, $_REPLACE);
    }
    public function setTypes(array $_ARRAY = null, bool $_REPLACE = false) {
	$this->frameTypes = call_user_func([$this, 'arrayMerge'], $this->frameTypes, $_ARRAY, $_REPLACE);
    }
    public function includeTypes(string $_INCLUDE, bool $_REPLACE = false) {
	$this->setTypes(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
}
