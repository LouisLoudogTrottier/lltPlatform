<?php
## GET #######################################################################################################
if(!($_TYPES=call_user_func([$LLTP,'getTypes'])) || !is_array($_TYPES)):
    call_user_func([$LLTP,'error'],['code'=>416]);
    unset($_TYPES); return; endif;
	
if(($_TYPE=call_user_func([$LLTP,'getRequests'],'type'))===null || $_TYPE === false):
    call_user_func([$LLTP,'error'],['code'=>400]);
    unset($_TYPES,$_TYPE); return; endif;
    
## RUN #######################################################################################################
$_FOUND=false;
foreach($_TYPES as $_CHECK):
    if(!isset($_CHECK['type'])): unset($_CHECK); continue; endif;
    if(!is_array($_CHECK['type'])): $_CHECK['type']=[$_CHECK['type']]; endif;
    $_CHECK['type']=array_map('strtolower', $_CHECK['type']);
    $_CHECK['type']=array_map('trim', $_CHECK['type'],['/.']);
	if(in_array($_TYPE, $_CHECK['type']) || in_array('*', $_CHECK['type'])):
		call_user_func([$LLTP,'setType'],$_CHECK,false);
		if(in_array($_TYPE, $_CHECK['type'])) $_FOUND=true;
	endif;
    unset($_CHECK); endforeach;
unseT($_TYPES,$_TYPE);

## END #######################################################################################################
if($_FOUND!==true) call_user_func ([$LLTP,'error'],['code'=>400]); 
if(!call_user_func([$LLTP,'getType'])) call_user_func ([$LLTP,'error'],['code'=>400]);
unset($_FOUND); return;