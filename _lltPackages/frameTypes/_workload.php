<?php
return [
    'request'=>[
		'nodes'=>[],
		'types'=>[
			__DIR__.'/request.php',
			__DIR__.'/workload.php',
			__DIR__.'/datas.php',
		],
    ],
    'content'=>[
		'types'=>[__DIR__.'/content.php'],
		'nodes'=>[],
    ],
	
];