<?php
## GET #######################################################################################################
if(!($_TYPE= call_user_func([$LLTP,'getType']))): 
    call_user_func ([$LLTP,'error'],['code'=>400]); 
    unset($_TYPE); return; endif;

## RUN #######################################################################################################
if(isset($_TYPE['workloads']) && is_array($_TYPE['workloads']))
    call_user_func ([$LLTP,'setWorkloads'],$_TYPE['workloads'],true);

if(isset($_TYPE['workflows'])) array_map([$LLTP,'runWorkflows']
    ,(is_array($_TYPE['workflows'])?$_TYPE['workflows']:[$_TYPE['workflows']]));
        
## END #######################################################################################################
unset($_TYPE);