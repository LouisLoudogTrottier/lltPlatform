<?php
## GET #######################################################################################################
if(!($_DATAS= call_user_func([$LLTP,'getType'],'datas'))): unset($_DATAS); return; endif;

## RUN #######################################################################################################
foreach($_DATAS as $_DATA=>$_GLOBS):
    @call_user_func([$LLTP,'debugStamp'],'[DATAS]['.$_DATA.'] '.$_GLOBS);
    foreach(call_user_func([$LLTP,'asGlobs'],$_GLOBS)?:[] as $_GLOB):
		call_user_func([$LLTP,'includeData'],$_DATA,$_GLOB);
	unset($_GLOB); endforeach; 
unset($_DATA,$_GLOBS);endforeach;

## END #######################################################################################################
unset($_DATAS);
return;