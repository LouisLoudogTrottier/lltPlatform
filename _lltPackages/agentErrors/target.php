<?php
## GET #######################################################################################################
if(!($_ERRORS=call_user_func([$LLTP,'getDatas'],'errors'))): unset($_ERRORS); return; endif;

## RUN #######################################################################################################
foreach($_ERRORS as $_ERROR):
	if(empty($_ERROR['name']) && empty($_ERROR['notify'])) continue;
	$_CONTENT[]='<a';
    if(!empty($_ERROR['state'])) $_CONTENT[]=' class="'.$_ERROR['state'].'"';
    $_CONTENT[]='>';
		if(!empty($_ERROR['code'])) $_CONTENT[]='['.$_ERROR['code'].'] ';
		$_CONTENT[]=implode(' : ', array_filter([
			(empty($_ERROR['name'])?'':'<b>'.$_ERROR['name'].'</b>'),
			(empty($_ERROR['notify'])?'':$_ERROR['notify']),
		]));
    $_CONTENT[]='</a>';
	call_user_func([$LLTP,'addContent'],'errors',implode($_CONTENT));
	unset($_ERROR,$_CONTENT);
endforeach;

## END #######################################################################################################
unset($_ERRORS);
return;
