<?php
return [
    400=>[
		'code'=>400,
		'name'=>'Bad Request',
		'state'=>'error',
		'workloads'=>[
			'request'=>[ 'type'=>null, ],
			'content'=>[ 'type'=>null, ],
		],
		#'workflows'=>__DIR__.'/fatal.php',
    ],
    403=>[
		'code'=>403,
		'name'=>'Forbidden',
		'state'=>'notice',
		'node'=>'login',
    ],
    404=>[
		'code'=>404,
		'name'=>'Not Found',
		'state'=>'error',
		'workflows'=>__DIR__.'/error.php',
    ],
    416=>[
		'code'=>416,
		'name'=>'Unavailable',
		'state'=>'notice',
    ],
    500=>[
		'code'=>500,
		'name'=>'Internal Error',
		'state'=>'error',
		'workflows'=>__DIR__.'/fatal.php',
    ],
    503=>[
		'code'=>503,
		'name'=>'Maintenance',
		'state'=>'notice',
		'notify'=>'In Maintenance, sorry if something seems broken.',
    ],
];
