<?php

trait agentErrors {

    private $agentErrors = [];

    private function __construct__agentErrors() {
		#@$this->includeagentErrors(__DIR__ . '/config.php');
		$this->__packages__agentErrors();
    }
    private function __packages__agentErrors() {
		global $_PACKAGES;
		foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_errors.php") as $_INCLUDE):
			$this->includeagentErrors($_INCLUDE);
			unset($_INCLUDE);
	    endforeach;
	    unset($_PACKAGE);
		endforeach;
    }
    public function getagentErrors(string $_ = null) {
		return call_user_func([$this, 'arrayRecurse'], $this->agentErrors, func_get_args());
    }
    public function setagentErrors(array $_ARRAY = null, bool $_REPLACE = true) {
		$this->agentErrors = call_user_func([$this, 'arrayMerge'], $this->agentErrors, $_ARRAY, $_REPLACE);
    }
    public function includeagentErrors(string $_INCLUDE, bool $_REPLACE = true) {
		$this->setagentErrors(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }

    public function error(array $_ARRAY){
		return $this->newError($_ARRAY);
    }public function newError(array $_ARRAY){
		$this->newErrorDefaults($_ARRAY);
		$this->newErrorLog($_ARRAY);
		$this->newErrorCode($_ARRAY);
		$this->newErrorName($_ARRAY);
		$this->newErrorWorkflows($_ARRAY);
		$this->newErrorWorkloads($_ARRAY);
		$this->newErrorDatas($_ARRAY);
		$this->newErrorStamp($_ARRAY);
    } private function newErrorDefaults(array &$_ARRAY){
		if(empty($_ARRAY['code'])) return;
		if(empty($this->agentErrors[$_ARRAY['code']])) return;
		if(is_array($this->agentErrors[$_ARRAY['code']]))
			$_ARRAY=array_replace_recursive($this->agentErrors[$_ARRAY['code']],$_ARRAY);
    } private function newErrorName(array &$_ARRAY){
		
		if(!empty($_ARRAY['name'])) call_user_func([$this,'setNode'],['name'=>$_ARRAY['name']],false);
    } private function newErrorCode(array &$_ARRAY){
		if(!empty($_ARRAY['code'])) http_response_code($_ARRAY['code']);
    } private function newErrorLog(array &$_ARRAY){
		if(!empty($_ARRAY['log'])) http_response_code($_ARRAY['log']);
    } private function newErrorDatas(array &$_ARRAY){
		if(isset($_ARRAY['code'])) 
			@call_user_func([$this,'setData'],'errors',[$_ARRAY['code']=>$_ARRAY]);
		else @call_user_func([$this,'setData'],'errors',[$_ARRAY]);
	} private function newErrorStamp(array &$_ARRAY){
		@call_user_func([$this,'debugStamp'],'[ERROR] '.json_encode($_ARRAY));	
    } private function newErrorWorkflows(array &$_ARRAY){
		if(isset($_ARRAY['workflows']) && is_array($_ARRAY['workflows']))
			call_user_func ([$this,'includeWorkflows'],$_ARRAY['workflows'],true);
    } private function newErrorWorkloads(array &$_ARRAY){
		if(isset($_ARRAY['workloads']) && is_array($_ARRAY['workloads']))
			call_user_func ([$this,'setWorkloads'],$_ARRAY['workloads'],true);
    }
}
