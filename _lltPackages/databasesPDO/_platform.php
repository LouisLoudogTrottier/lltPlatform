<?php

trait databasesPDO {

    private $PDO = [];
    private $databasesPDO = [];

    private function __construct__databasesPDO() {
		$this->__packages__databasesPDO();
    }

    private function __packages__databasesPDO() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_htmysql") as $_INCLUDE):
	    $this->includeDatabases($_INCLUDE); unset($_INCLUDE); endforeach;
	    unset($_PACKAGE); endforeach; 
    }
    public function getDatabases(string $_ = null) {
        return call_user_func([$this, 'arrayRecurse'], $this->databasesPDO, func_get_args());
    }
    public function setDatabases(array $_ARRAY = null, bool $_REPLACE = true) {
        $this->databasesPDO = call_user_func([$this, 'arrayMerge'], $this->databasesPDO, $_ARRAY, $_REPLACE);
    }
    public function includeDatabases(string $_INCLUDE, bool $_REPLACE = true) {
        $this->setDatabases(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
    public function newPDO(string $_KEY=null){
        if(empty($_KEY) || empty($this->databasesPDO[$_KEY])) return;
        if(!($_ARRAY = call_user_func([$this,'includeArray'],$this->databasesPDO[$_KEY]))) return;
        if(empty($_ARRAY['host']) || empty($_ARRAY['user']) || empty($_ARRAY['pass'])) return;

        $R[]='mysql:';
        $R[]='host='.$_ARRAY['host'].';';
        if(!empty($_ARRAY['name'])) $R[]='dbname='.$_ARRAY['name'].';';
        $R[]='charset='.(empty($_ARRAY['charset'])?'utf8': $_ARRAY['charset']).';';

        try{
            $this->PDO['PDO']=new PDO(implode($R), $_ARRAY['user'], $_ARRAY['pass']);
            call_user_func([$this->PDO['PDO'],'setAttribute'],PDO::ATTR_EMULATE_PREPARES, false );
            call_user_func([$this->PDO['PDO'],'setAttribute'],PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $_EX) {
            call_user_func([$this,'error'],['code'=>500,'log'=>$_EX]);
            $this->PDO['errors'][]=$_EX;
        }
        #return $this->PDO['PDO'];
    }
    public function setPDO(string $_QUERY=null){
        if(empty($this->PDO['PDO'])) return;
        try{
            $this->PDO['stmt']=call_user_func([$this->PDO['PDO'],'prepare'],$_QUERY);
        } catch (Exception $_EX) {
            call_user_func([$this,'error'],['code'=>500,'log'=>$_EX]);
            $this->PDO['errors'][]=$_EX;
        }
    }public function runPDO(array $_VALUES=null){
	if(empty($this->PDO['stmt'])) return;
	try{
	    call_user_func([$this->PDO['stmt'],'execute'],$_VALUES);
	    $this->PDO['insert']=call_user_func([$this->PDO['PDO'],'lastInsertId']); 
	    $this->PDO['update']=call_user_func([$this->PDO['stmt'],'rowCount']); 
	    $this->PDO['select']=call_user_func([$this->PDO['stmt'],'fetchAll'],PDO::FETCH_ASSOC);
	} catch (Exception $_EX) {
	    call_user_func([$this,'error'],['code'=>500,'log'=>$_EX]);
	    $this->PDO['errors'][]=$_EX;
	}
    }public function getPDO(){
		@$this->PDO['errors']['PDO']=call_user_func([$this->PDO['PDO'],'errorInfo']);
		@$this->PDO['errors']['stmt']=call_user_func([$this->PDO['stmt'],'errorInfo']);
		return call_user_func([$this,'arrayRecurse'],$this->PDO, func_get_args());
    }   
}
