<?php
## GET #######################################################################################################
if(!($_CONTENTS= call_user_func([$LLTP,'getNode'],'contents'))): unset($_CONTENTS); return; endif;

## RUN #######################################################################################################
foreach($_CONTENTS as $_CONTENT=>$_GLOBS):
    @call_user_func([$LLTP,'debugStamp'],'[CONTENTS]['.$_CONTENT.'] '.$_GLOBS);
    ob_start(); 
    foreach(call_user_func([$LLTP,'asGlobs'],$_GLOBS)?:[] as $_GLOB): 
		include $_GLOB;
		unset($_GLOB); endforeach;
    call_user_func([$LLTP,'addContent'],$_CONTENT, ob_get_contents());
    ob_end_clean();
	unset($_CONTENT,$_GLOBS); endforeach;
## END #######################################################################################################
unset($_CONTENTS);
return;