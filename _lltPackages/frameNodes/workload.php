<?php
if(!($_NODE= call_user_func([$LLTP,'getNode']))): 
    call_user_func ([$LLTP,'error'],['code'=>400]); 
    unset($_NODE); return; endif;

if(isset($_NODE['workloads']) && is_array($_NODE['workloads']))
    call_user_func ([$LLTP,'setWorkloads'],$_NODE['workloads'],true);

if(isset($_NODE['workflows'])) array_map([$LLTP,'runWorkflows']
    ,(is_array($_NODE['workflows'])?$_NODE['workflows']:[$_NODE['workflows']]));
        
unset($_NODE);