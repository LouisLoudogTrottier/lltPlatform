<?php
## GET #######################################################################################################
if(!($_NODES=call_user_func([$LLTP,'getNodes'])) || !is_array($_NODES)):
    call_user_func([$LLTP,'error'],['code'=>416]);
    unset($_NODES); return; endif;
    
if(($_LINK=call_user_func([$LLTP,'getRequests'],'link'))===null || $_LINK === false):
    call_user_func([$LLTP,'error'],['code'=>400]);
    unset($_NODES,$_LINK); return; endif;
    
## RUN #######################################################################################################
$FOUND=false;
foreach($_NODES as $_CHECK):
    if(!isset($_CHECK['link'])): unset($_CHECK); continue; endif;
    if(!is_array($_CHECK['link'])): $_CHECK['link']=[$_CHECK['link']]; endif;
    #$_CHECK['link']=array_map('strtolower', $_CHECK['link']);
    $_CHECK['link']=array_map('trim', $_CHECK['link'],['/.']);
    if(in_array($_LINK, $_CHECK['link'])):
		@call_user_func([$LLTP,'debugStamp'],'[NODE] '.json_encode($_CHECK));
		call_user_func([$LLTP,'setNode'],$_CHECK,false);
		$FOUND=true;
    endif;
    unset($_CHECK); endforeach;      
unseT($_NODES,$_LINK);

## END #######################################################################################################
if($FOUND!==true ) call_user_func ([$LLTP,'error'],['code'=>404]); 
if(!call_user_func([$LLTP,'getNode']) )call_user_func ([$LLTP,'error'],['code'=>404]); 
unset($FOUND); return;