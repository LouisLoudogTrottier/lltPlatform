<?php

return [
    'request' => [
	'nodes'=>[
	    __DIR__.'/request.php',
	    __DIR__.'/workload.php',
	    __DIR__.'/datas.php',
	],
	'types'=>[],
    ],
    'content' => [
	'types'=>[],
	'nodes'=>[__DIR__.'/content.php',],	
    ],
];
