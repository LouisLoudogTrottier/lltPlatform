<?php

trait frameNodes {

    private $frameNode = [];
    private $frameNodes = [];

    private function __construct__frameNodes() {
		#@$this->includeNodes(__DIR__ . '/nodes.php');
		$this->__packages__frameNodes();
    }
    private function __packages__frameNodes() {
		global $_PACKAGES;
		foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_nodes.php") as $_INCLUDE):
			$this->includeNodes($_INCLUDE); unset($_INCLUDE); endforeach;
			unset($_PACKAGE); endforeach;
    }

    public function getNode(string $_ = null) {
		return call_user_func([$this, 'arrayRecurse'], $this->frameNode, func_get_args());
    }
    public function getNodes(string $_ = null) {
		return call_user_func([$this, 'arrayRecurse'], $this->frameNodes, func_get_args());
    }

    public function setNode(array $_ARRAY = null, bool $_REPLACE = true) {
		$this->frameNode = call_user_func([$this, 'arrayMerge'], $this->frameNode, $_ARRAY, $_REPLACE);
    }
    public function setNodes(array $_ARRAY = null, bool $_REPLACE = true) {
		$this->frameNodes = call_user_func([$this, 'arrayMerge'], $this->frameNodes, $_ARRAY, $_REPLACE);
    }
    
    public function includeNodes(string $_INCLUDE, bool $_REPLACE = true) {
		$this->setNodes(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
}
