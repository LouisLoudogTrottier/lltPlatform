<?php
## GET #######################################################################################################
if(!($_DATAS= call_user_func([$LLTP,'getNode'],'datas'))): unset($_DATAS); return; endif;

## RUN #######################################################################################################
foreach($_DATAS as $_DATA=>$_GLOBS):
    @call_user_func([$LLTP,'debugStamp'],'[DATAS]['.$_DATA.'] '.$_GLOBS);
    foreach(call_user_func([$LLTP,'asGlobs'],$_GLOBS)?:[] as $_GLOB):
		@call_user_func([$LLTP,'debugStamp'],'[DATAS]['.$_DATA.'] '.$_GLOB);
		#call_user_func([$LLTP,'includeData'],$_DATA,$_GLOB);
		if(($_ARRAY=include $_GLOB) && is_array($_ARRAY))
			call_user_func([$LLTP,'setData'],$_DATA,$_ARRAY);
	unset($_GLOB); endforeach; 
unset($_DATA,$_GLOBS);endforeach;

## END #######################################################################################################
unset($_DATAS);
return;
