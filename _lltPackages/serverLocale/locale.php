<?php
if(function_exists('mb_internal_encoding')):
    mb_internal_encoding(call_user_func([$LLTP,'getConfigs'],'locale','charset')?:'utf-8');
    @call_user_func([$LLTP,'debugStamp'], '- charset : '.mb_internal_encoding());
endif;

if(function_exists('date_default_timezone_set')):
    date_default_timezone_set(call_user_func([$LLTP,'getConfigs'],'locale','timezone')?:'UTC');
    @call_user_func([$LLTP,'debugStamp'], '- timezone : '.date_default_timezone_get());
endif;
