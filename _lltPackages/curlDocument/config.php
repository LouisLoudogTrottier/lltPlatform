<?php

return [
    /*
    'metas'=>[
	'name'=>'content',
	'charset'=>'charset',
	'http-equiv'=>'content',
	'property'=>'content',
	'itemprop'=>'content',
    ],
    'names'=>[
	'title'=>'title',
	'og:title'=>'og:title',
	'twitter:title'=>'twitter:title',
    ],
    'notes'=>[
	'description'=>'description',
	'og:description'=>'og:description',
	'twitter:description'=>'twitter:description',
	'p'=>'p',
    ],
    'icons'=>[
	'image'=>'image',
	'og:image'=>'og:image',
	'twitter:image'=>'twitter:image',
	'favicon'=>'favicon',
    ],
    'strip'=>[
	'__xts__[0]',
	'__xts__[1]',
	'__tn__',
	'hc_ref',
	'fbclid',
	'theater',
	'app',
	'drole.quebec',
    ],
    */
];