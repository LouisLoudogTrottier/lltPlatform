<?php
trait curlDocument {
    private $curlDocument=[];
    
    public function getCurlDocument(string $k='DOM'){ return $this->curlDocument[$k]; }
    public function setCurlDocument(array $curl,bool $replace=true){
	$this->curlDocument=$replace
	    ? array_replace_recursive($this->curlDocument,$curl)
	    : array_merge_recursive($this->curlDocument,$curl);
    }
    public function runCurlDocument(){
	try{
	    $this->runCurlDocumentDOM();
	    $this->runCurlDocumentHref();
	    #$this->runCurlDocumentMeta();
	    $this->runCurlDocumentImg();
	    
	} catch (Exception $ex) {
	    error_log($ex);
	}
    }
    private function runCurlDocumentDOM(){
	call_user_func([$this,'setCurl'],$this->curlDocument);
	call_user_func([$this,'runCurl']);
	$this->curlDocument['DOM']=new DOMDocument();
	@$this->curlDocument['DOM']->loadHTML(call_user_func([$this,'getCurl']));	
    }
    private function runCurlDocumentHref(){
	if(empty($this->curlDocument['DOM'])) return;
	if(!($this->curlDocument['a']=$this->curlDocument['DOM']->getElementsByTagName('a'))) return;
	foreach($this->curlDocument['a'] as $k=>$v):
	    if(($vv=$v->getAttribute('href'))) $this->curlDocument['href'][]=trim($vv);
	    unset ($k,$v,$vv);
	endforeach;
    }
    private function runCurlDocumentImg(){
	if(empty($this->curlDocument['DOM'])) return;
	$this->curlDocument['img']=$this->curlDocument['DOM']->getElementsByTagName('img');
	foreach($this->curlDocument['img'] as $k=>$v):
	    if(($vv=$v->getAttribute('src'))) $this->curlDocument['src'][]=trim($vv);
	    unset ($k,$v,$vv);
	endforeach;
	
    }
    /*private function runCurlDocumentMeta(){
	if(empty($this->curlDocument['DOM'])) return;
	$this->curlDocument['meta']=$this->curlDocument['DOM']->getElementsByTagName('meta');
    }*/
    
}

    /*
    // *********************************************************************************************
    private function _crawlHrefs(){
        if(empty($this->crawlDocument) ) return false;
        if(!($htmls=$this->crawlDocument->getElementsByTagName('a'))) return false;
        foreach($htmls as $href){
            if(($vv=$href->getAttribute('href'))){ $this->crawl['hrefs'][]=trim($vv); } }
        return @$this->crawl['hrefs'];
    }
    private function _crawlIcons(){
        if(empty($this->crawlDocument) ) return false;
        if(!($htmls=$this->crawlDocument->getElementsByTagName('img'))) return false;
        $this->crawl['icons']['icon']='favicon.ico';
        foreach($htmls as $img){
            if(($vv=$img->getAttribute('src')) &&
                substr(strtolower($vv), 0,5)!='data:' &&
                substr(strtolower($vv), -4)!='.svg' &&
                true ){ $this->crawl['icons'][]=trim($vv); } }
        return @$this->crawl['icons'];
    }
    private function _crawlMetas(){
        if(empty($this->crawlDocument) ) return false;
        if(!($htmls=$this->crawlDocument->getElementsByTagName('meta'))) return false;
        $known=call_user_func([$this,'_config'],'crawl','metas')?:[];
        // -----------------------------------------------------------------------------------------
        foreach($htmls as $m=>$meta){
            foreach($known as $k=>$v){
                if(($kk=$meta->getAttribute($k)) && ($vv=$meta->getAttribute($v))){
                    if($k==$v) $kk=$k;
                    $this->crawl['metas'][strtolower(trim($kk))]=trim($vv);
                    $found=true;
                }unset($k,$v,$kk,$vv,$m); }
            if(empty($found)) $this->crawl['metas'][$m]=trim($this->crawlDocument->saveHTML($meta));
        }
        return @$this->crawl['metas'];
    }
    private function _crawlTags(){
        if(empty($this->crawlDocument) ) return false;
        // ----------------------------------------------------------------------------------------
        if(($vv=@$this->crawlDocument->getElementsByTagName('title')[0]->nodeValue)
            ) $this->crawl['tags']['title']=trim($vv);
        if(($vv=@$this->crawlDocument->getElementsByTagName('h1')[0]->nodeValue)
            ) $this->crawl['tags']['h1']=trim($vv);
        if(($vv=@$this->crawlDocument->getElementsByTagName('p')[0]->nodeValue)
            ) $this->crawl['tags']['p']=trim($vv);
        return @$this->crawl['tags'];
    }
    // *********************************************************************************************
    public function _metaName(){
        $known=call_user_func([$this,'_config'],'crawl','names')?:[];
        foreach($known as $k){$k=strtolower($k);
            if(isset($this->crawl['metas'][$k])) return $this->crawl['metas'][$k];
            if(isset($this->crawl['tags'][$k])) return $this->crawl['tags'][$k];
            unset($k);}
    }
    public function _metaNote(){
        $known=call_user_func([$this,'_config'],'crawl','notes')?:[];
        foreach($known as $k){$k=strtolower($k);
            if(isset($this->crawl['metas'][$k])) return $this->crawl['metas'][$k];
            if(isset($this->crawl['tags'][$k])) return $this->crawl['tags'][$k];
            unset($k);}
    }
    public function _metaIcon(){
        $known=call_user_func([$this,'_config'],'crawl','icons')?:[];
        foreach($known as $k){ $k=strtolower($k);
            if(isset($this->crawl['metas'][$k])) return $this->crawl['metas'][$k];
            if(isset($this->crawl['tags'][$k])) return $this->crawl['tags'][$k];
            unset($k);}
        return empty($this->crawl['icons'])?'/favicon.ico':reset($this->crawl['icons']);
    }
     * 
     */