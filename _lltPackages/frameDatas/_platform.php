<?php

trait frameDatas {

    private $frameDatas = [];

    private function __construct__frameDatas() {
		#@$this->includeDatas(__DIR__ . '/config.php');
		$this->__packages__frameDatas();
    }

    private function __packages__frameDatas() {
		global $_PACKAGES;
		foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_datas.php") as $_INCLUDE):
			$this->includeDatas($_INCLUDE); unset($_INCLUDE); endforeach;
			unset($_PACKAGE); endforeach;
    }
    public function getData(string $_ = null) {
		return call_user_func([$this, 'arrayRecurse'], $this->frameDatas, func_get_args());
	}
    public function getDatas(string $_ = null) {
		return call_user_func([$this, 'arrayRecurse'], $this->frameDatas, func_get_args());
    }
    public function setData(string $_DATA, array $_ARRAY = null, bool $_REPLACE = true) {
		$this->setDatas([$_DATA=>$_ARRAY],$_REPLACE);
    }
    public function setDatas(array $_ARRAY = null, bool $_REPLACE = false) {
		$this->frameDatas = call_user_func([$this, 'arrayMerge'], $this->frameDatas, $_ARRAY, $_REPLACE);
    }

    public function includeData(string $_DATA, string $_INCLUDE, bool $_REPLACE = true) {
		if(($_ARRAY=call_user_func([$this, 'includeArray'], $_INCLUDE))) $this->setData($_DATA,$_ARRAY, $_REPLACE);  unset($_ARRAY);
	}
    
	public function includeDatas(string $_INCLUDE, bool $_REPLACE = true) {
		$this->setDatas(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
}
