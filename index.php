<?php 
## PACKAGES ##################################################################################################
$_PACKAGES['__debugs']=__DIR__.'/__devDebugs';
$_PACKAGES['_plugins']=__DIR__.'/_lltPlugins/*/';
$_PACKAGES['_prefabs']=__DIR__.'/_lltPrefabs/*/';
$_PACKAGES['DEMO']    =__DIR__.'/DEMO';

## PLATFORM ##################################################################################################
require_once __DIR__.'/_platform.php';

## WORKLOAD ##################################################################################################
while($_WORKLOAD=call_user_func([$LLTP,'runWorkloads'])):
    @call_user_func([$LLTP,'debugStamp'], '[WORKLOAD] '.$_WORKLOAD);
    include_once $_WORKLOAD; 
    unset($_WORKLOAD); endwhile; 

## CLEAN UP ##################################################################################################
unset($_WORKLOAD,$LLTP); 
exit(); 