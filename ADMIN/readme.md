# Louis Loudog Trottier
## PHP Packaged Homemade Platform

### Demonstrate how you can load more package using folder
1. Add an `.htaccess` with the rewrite base
   - Optionally add the basic auth 
```
RewriteEngine On
RewriteBase /ADMIN/
```
2. Add an `index.php` file with the required packages 
   ```
$_PACKAGES['admins']=__DIR__.'/';        
$_PACKAGES['debugs']=__DIR__.'/../__devDebugs'; 
```

### For this demonstration with basic auth use :
- username : admin
- password : demo
