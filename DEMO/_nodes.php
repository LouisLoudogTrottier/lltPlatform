<?php
return[
    'DEMO'=>[
		'link'=>'/DEMO',
		'name'=>'Simple DEMO',
		'scripts'=>[ 'DEMO'=>__DIR__.'/_*.js', ],
		'workloads'=>['DEMO'=>__DIR__.'/workload.php'], 
    ],
];