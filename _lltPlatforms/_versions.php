<?php
return [
    'author'=>'LouisLoudogTrottier',
    'version'	=>'0.'.date('y.m'),
    'release'	=>'alpha',
    'build'	=>date('dhi',filemtime(__FILE__)),
];