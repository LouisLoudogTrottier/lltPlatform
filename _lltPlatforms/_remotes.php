<?php
return array_merge(
	function_exists('apache_request_headers')?apache_request_headers():[],
    ['addrs'=>array_filter([
		 filter_input(INPUT_SERVER, 'REMOTE_ADDR'),
		 filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP'),
		 filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR'),
		 filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED'),
		 filter_input(INPUT_SERVER, 'HTTP_FORWARDED_FOR'),
		 filter_input(INPUT_SERVER, 'HTTP_FORWARDED'),
    ]),]
);
