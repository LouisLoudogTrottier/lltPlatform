<?php
return[
    'uri'=>filter_input(INPUT_SERVER,'REQUEST_URI',FILTER_SANITIZE_URL),
    'get'=> filter_input_array(INPUT_GET),
    'post'=> filter_input_array(INPUT_POST),
    #'request'=> filter_input_array(INPUT_REQUEST),
    'http'=> filter_input(INPUT_SERVER,'HTTPS')?'https':'http',
    'host'=> strtolower(filter_input(INPUT_SERVER,'HTTP_HOST')),
];
