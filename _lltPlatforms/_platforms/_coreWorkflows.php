<?php

trait _coreWorkflows {

    private $_coreWorkflows = [];

    ## #############################################################################################
    private function __construct___coreWorkflows() {
	#@$this->includeWorkflows(__DIR__ . '/config.php');
	$this->__packages___coreWorkflows();
    }

    ## #############################################################################################
    private function __packages___coreWorkflows() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_workflow.php") as $_INCLUDE):
	    @call_user_func([$this,'debugStamp'],'[_WORKFLOW] '.$_INCLUDE);
	    $this->includeWorkflows($_INCLUDE); unset($_INCLUDE); endforeach;
	    unset($_PACKAGE); endforeach;
    }

    ## #############################################################################################
    public function getWorkflows(string $_ = null) {
	return call_user_func([$this, 'arrayRecurse'], $this->_coreWorkflows, func_get_args());
    }
    ## #############################################################################################
    public function setWorkflows(array $_ARRAY = null, bool $_REPLACE = true) {
	$this->_coreWorkflows = call_user_func([$this, 'arrayMerge'], $this->_coreWorkflows, $_ARRAY, $_REPLACE);
    }
    ## #############################################################################################
    public function includeWorkflows(string $_INCLUDE, bool $_REPLACE = true) {
	$this->setWorkflows(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
    ## #############################################################################################
    public function runWorkflows(string $_KEY,bool $_REPLACE=false) {
	if(isset($this->_coreWorkflows[$_KEY]))
	    call_user_func([$this,'includeWorkloads'],$this->_coreWorkflows[$_KEY],$_REPLACE);
    }
}
