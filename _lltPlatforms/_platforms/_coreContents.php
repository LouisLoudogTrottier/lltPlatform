<?php

trait _coreContents {

    private $frameContents = [];

    public function getContent(string $_KEY, $_GLUE=' ', bool $_RETURN=true){
		if($_GLUE===false): $_GLUE=' '; $_RETURN=false; endif;
		if(!@is_array($this->frameContents[$_KEY])) $this->frameContents[$_KEY]=[];
		$_R=implode($_GLUE,array_filter($this->frameContents[$_KEY]));
		if($_RETURN) return $_R; else echo $_R;
    } public function addContent(string $_KEY, string $_VALUE=null){
		if(!@is_array($this->frameContents[$_KEY])) $this->frameContents[$_KEY]=[];
		if($_VALUE) array_push($this->frameContents[$_KEY], trim($_VALUE));
    } public function setContent(string $_KEY, string $_VALUE=null){
		if(!@is_array($this->frameContents[$_KEY])) $this->frameContents[$_KEY]=[];
		if($_VALUE) $this->frameContents[$_KEY]=[trim($_VALUE)];
    } public function topContent(string $_KEY, string $_VALUE=null){
		if(!@is_array($this->frameContents[$_KEY])) $this->frameContents[$_KEY]=[];
		if($_VALUE) array_unshift($this->frameContents[$_KEY], trim($_VALUE));
    }
}