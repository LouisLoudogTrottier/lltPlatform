<?php

trait _bareVersions {

    private $_bareVersions = [];

    private function __construct___bareVersions() {
		$this->__packages___bareVersions();
    }
    ## #############################################################################################
    private function __packages___bareVersions() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_versions.php") as $_INCLUDE):
	    @call_user_func([$this,'debugStamp'],'[_versions] '.$_INCLUDE);
	    $this->includeVersions($_INCLUDE); unset($_INCLUDE); endforeach;
	    unset($_PACKAGE); endforeach;
    }

    public function getVersions() {
		return call_user_func([$this, 'arrayRecurse'], $this->_bareVersions, func_get_args());
    }

    public function setVersions(array $_ARRAY = null, bool $_REPLACE = true) {
		$this->_bareVersions = call_user_func([$this, 'arrayMerge'], $this->_bareVersions, $_ARRAY, $_REPLACE);
    }

    public function includeVersions(string $_INCLUDE, bool $_REPLACE = true) {
		$this->setVersions(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
}
