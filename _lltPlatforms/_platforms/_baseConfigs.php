<?php
trait _baseConfigs {
    private $_baseConfigs = [];
    ## #############################################################################################
    private function __construct___baseConfigs() {
	$this->__packages___baseConfigs();
    }
    ## #############################################################################################
    private function __packages___baseConfigs() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_configs.php") as $_INCLUDE):
	    @call_user_func([$this,'debugStamp'],'[_CONFIGS] '.$_INCLUDE);
	    $this->includeConfigs($_INCLUDE); unset($_INCLUDE); endforeach;
	    unset($_PACKAGE); endforeach;
    }
    ## #############################################################################################
    public function getConfigs(string $_ = null) {
	return call_user_func([$this, 'arrayRecurse'], $this->_baseConfigs, func_get_args());
    }
    ## #############################################################################################
    public function setConfigs(array $_ARRAY = null, bool $_REPLACE = true) {
	$this->_baseConfigs = call_user_func([$this, 'arrayMerge'], $this->_baseConfigs, $_ARRAY, $_REPLACE);
    }
    ## #############################################################################################
    public function includeConfigs(string $_INCLUDE, bool $_REPLACE = true) {
	$this->setConfigs(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
}
