<?php

trait _coreWorkloads {

    private $_coreWorkloads = [];

    private function __construct___coreWorkloads() {
		@$this->includeWorkloads(__DIR__ . '/config.php');
		$this->__packages___coreWorkloads(); }
    private function __packages___coreWorkloads() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_workload.php") as $_INCLUDE):
	    @call_user_func([$this,'debugStamp'],'[_WORKLOADS] '.$_INCLUDE);
	    $this->includeWorkloads($_INCLUDE); unset($_INCLUDE); endforeach;
	    unset($_PACKAGE); endforeach; }
    public function getWorkloads(string $_ = null) {
		return call_user_func([$this, 'arrayRecurse'], $this->_coreWorkloads, func_get_args()); }
    public function setWorkloads(array $_ARRAY = null, bool $_REPLACE = true) {
	$this->_coreWorkloads = call_user_func([$this, 'arrayMerge'], 
	    $this->_coreWorkloads, $_ARRAY, $_REPLACE); }
    public function includeWorkloads(string $_INCLUDE, bool $_REPLACE = true) {
		$this->setWorkloads(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE); }
    public function runWorkloads() {
		do{ $_WORKLOAD=$this->runWorkloadsShift($this->_coreWorkloads);
		}while(isset($_WORKLOAD) && is_array($_WORKLOAD));
		return $_WORKLOAD; } 	
    private function runWorkloadsShift(array &$_WORKLOADS=[]){
	foreach($_WORKLOADS as &$_WORKLOAD){
	    return (is_array($_WORKLOAD) && !empty($_WORKLOAD))
			? $this->runWorkloadsShift($_WORKLOAD) 
			: (array_shift($_WORKLOADS)?:[]); 
		}
    }
}
