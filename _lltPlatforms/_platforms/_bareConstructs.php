<?php
trait _bareConstructs {
    ## #############################################################################################
    public function __construct() {
        @call_user_func([$this, 'debugStamp'], __FUNCTION__);
        $this->runConstructs();
    } 
    ## #############################################################################################
    private function runConstructs(){
        foreach(get_class_methods(get_class($this)) as $_METHOD):
	    if(strlen($_METHOD)>13 && substr($_METHOD, 0, 13)=='__construct__'):
		@call_user_func([$this, 'debugStamp'], $_METHOD);
		call_user_func([$this,$_METHOD]);
	    endif; 
	    unset($_METHOD); 
	endforeach;
    }   
}