<?php
trait _bareCommons{
    ## #############################################################################################
    public static function asArray($_AS=null){ return is_array($_AS) ? $_AS : [$_AS]; }
    public static function asReset($_AS=null){ return is_array($_AS) ? reset($_AS) : $_AS; }
    public static function asEnd($_AS=null){ return is_array($_AS) ? end($_AS) : $_AS; }
    public static function asFirst($_AS=null){ return is_array($_AS) ? self::asReset(array_filter($_AS)):$_AS; }
    public static function asLast($_AS=null){ return is_array($_AS) ? self::asEnd(array_filter($_AS)) :$_AS; }
        
    ## #############################################################################################
    public static function asGlobs($_GLOBS=null){
	$_R=[];
	foreach(self::asArray($_GLOBS) as $_GLOB):
	    $_R= array_merge($_R,glob($_GLOB));
	    unset($_GLOB); endforeach;
		unset($_GLOBS);
		return self::asArray($_R); }
		
    ## #############################################################################################
    public static function arrayRecurse(array $_ARRAY, array $_KEYS=null){
		if(empty($_ARRAY)) return;
		if(isset($_KEYS) && is_array($_KEYS)): 
			foreach($_KEYS as $_K):
			$_ARRAY= is_array($_ARRAY) && key_exists($_K, $_ARRAY)?$_ARRAY[$_K]:null; 
			unset($_K); endforeach;
			endif; unset($_KEYS);     
		return $_ARRAY; }
    
    ## #############################################################################################
    public static function arrayMerge(array $_ARRAY1, array $_ARRAY2, bool $_REPLACE=false){
	return $_REPLACE 
	    ? array_replace_recursive($_ARRAY1, $_ARRAY2)
	    : array_merge_recursive($_ARRAY1, $_ARRAY2); }
    
    ## #############################################################################################
    public static function includeArray(string $_FILE){
		return (($_ARRAY=include $_FILE) && is_array($_ARRAY)) ? $_ARRAY : []; }
}