<?php

trait _baseRemotes {

    private $_baseRemotes = [];
    ## #############################################################################################
    private function __construct___baseRemotes() {

	$this->__packages___baseRemotes();
	$this->runRemotes();
    }
    ## #############################################################################################
    private function __packages___baseRemotes() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_remotes.php") as $_INCLUDE):
	    @call_user_func([$this,'debugStamp'],'[_remotes] '.$_INCLUDE);
	    $this->includeRemotes($_INCLUDE); unset($_INCLUDE); endforeach;
	    unset($_PACKAGE); endforeach;
    
    }
    
    ## #############################################################################################
    public function getRemotes(string $_ = null) {
	return call_user_func([$this, 'arrayRecurse'], $this->_baseRemotes, func_get_args());
    }
    ## #############################################################################################
    public function setRemotes(array $_ARRAY = null, bool $_REPLACE = true) {
	$this->_baseRemotes = call_user_func([$this, 'arrayMerge'], $this->_baseRemotes, $_ARRAY, $_REPLACE);
    }

    ## #############################################################################################
    public function includeRemotes(string $_INCLUDE, bool $_REPLACE = true) {
	$this->setRemotes(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
    ## #############################################################################################
    private function runRemotes() {
		if(isset($this->_baseRemotes['Accept']) && is_array($this->_baseRemotes['Accept']))
			$this->_baseRemotes['Accept']=explode(',', $this->_baseRemotes['Accept']);
		if(isset($this->_baseRemotes['Accept-Encoding']) && is_array($this->_baseRemotes['Accept-Encoding']))
			$this->_baseRemotes['Accept-Encoding']= explode(',', $this->_baseRemotes['Accept-Encoding']);
		if(isset($this->_baseRemotes['Accept-Language']) && is_array($this->_baseRemotes['Accept-Language']))
			$this->_baseRemotes['Accept-Language']= explode(',', $this->_baseRemotes['Accept-Language']);
    }
}
