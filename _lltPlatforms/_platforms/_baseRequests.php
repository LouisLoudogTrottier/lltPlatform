<?php

trait _baseRequests {

    private $_baseRequests = [];

    ## #############################################################################################
    private function __construct___baseRequests() {
	$this->__packages___baseRequests();
	$this->runRequests();
    }
    private function __packages___baseRequests() {
	global $_PACKAGES;
	foreach ($_PACKAGES as $_PACKAGE): foreach (glob($_PACKAGE . "/_requests.php") as $_INCLUDE):
	    @call_user_func([$this,'debugStamp'],'[_requests] '.$_INCLUDE);
	    $this->includeRequests($_INCLUDE); unset($_INCLUDE); endforeach;
	    unset($_PACKAGE); endforeach;
    }
    ## #############################################################################################
    public function getRequests(string $_ = null) {
	return call_user_func([$this, 'arrayRecurse'], $this->_baseRequests, func_get_args());
    }
    ## #############################################################################################
    public function setRequests(array $_ARRAY = null, bool $_REPLACE = true) {
	$this->_baseRequests = call_user_func([$this, 'arrayMerge'], $this->_baseRequests, $_ARRAY, $_REPLACE);
    }
    ## #############################################################################################
    public function includeRequests(string $_INCLUDE, bool $_REPLACE = true) {
	$this->setRequests(call_user_func([$this, 'includeArray'], $_INCLUDE), $_REPLACE);
    }
    ## #############################################################################################
    private function runRequests(){
	$this->_baseRequests['url']=trim(parse_url($this->_baseRequests['uri'], PHP_URL_PATH),'/');
	$this->_baseRequests['path']=trim(pathinfo($this->_baseRequests['url'], PATHINFO_DIRNAME),'/.');
	$this->_baseRequests['file']=trim(pathinfo($this->_baseRequests['url'], PATHINFO_FILENAME),'/.');
	$this->_baseRequests['type']=trim(pathinfo($this->_baseRequests['url'], PATHINFO_EXTENSION),'/.');
	$this->_baseRequests['link']=trim($this->_baseRequests['path'].'/'.$this->_baseRequests['file'],'/.');
	#$this->_baseRequests['base']=trim($this->_baseRequests['http'].'://'.$this->_baseRequests['host'],'/.');	
	#$this->_baseRequests['href']=trim($this->_baseRequests['base'].'/'.$this->_baseRequests['link'],'/.');	
    }
}
