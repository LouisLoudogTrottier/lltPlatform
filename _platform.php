<?php 
## DEFINE ################################################################################
if(!defined('LLTP_ROOT')) define('LLTP_ROOT',__DIR__);

## PACKAGES ##############################################################################
if(!(isset($_PACKAGES)	&& is_array($_PACKAGES)))   $_PACKAGES=[];
array_unshift($_PACKAGES,LLTP_ROOT.'/_lltPackages/*/');
array_unshift($_PACKAGES,LLTP_ROOT.'/_lltPlatforms');

## PLATFORM ##############################################################################
foreach($_PACKAGES as $_PACKAGE):
	
    ## ===================================================================================
    foreach(glob($_PACKAGE."/_platform.php") as $_PLATFORM):
		include_once $_PLATFORM;
		$_TRAIT=basename(pathinfo($_PLATFORM, PATHINFO_DIRNAME));
		if(trait_exists($_TRAIT)) $_TRAITS[]=$_TRAIT;
		unset($_PLATFORM,$_TRAIT); endforeach;
	
    ## ===================================================================================
    foreach(glob($_PACKAGE."/_platforms/*.php") as $_PLATFORM):
		include_once $_PLATFORM;
		$_TRAIT=pathinfo(basename($_PLATFORM), PATHINFO_FILENAME);
		if(trait_exists($_TRAIT)) $_TRAITS[]=$_TRAIT;
		unset($_PLATFORM,$_TRAIT); endforeach;
		
    unset ($_PACKAGE);
endforeach;

## EVAL ##################################################################################
eval("class lltPlatform{".(!empty($_TRAITS) && is_array($_TRAITS)
	? " use ".implode(',',array_unique($_TRAITS)).";"
	: '')."}"); 
	unset($_TRAITS);

## CONSTRUCT #############################################################################
$LLTP=new lltPlatform(); 

## CLEANUP ###############################################################################
unset($_PACKAGES);