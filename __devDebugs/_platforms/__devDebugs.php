<?php

trait __devDebugs {
    private $__devDebugs = [];
    public function __construct____devDebugs(){ @call_user_func([$this,'error'],['code'=>503]);}
    ## #############################################################################################
	
    public function debugField(string $_FIELD, $_RETURN=false){
		$_R=isset($this->{$_FIELD})? $_R=$this->debugPre($this->{$_FIELD},true):'';
        if($_RETURN) return $_R; echo $_R; } 
		
	static public function debugPre($_VALUE,$_RETURN=false){
        $_R="<pre>".self::debugPrint($_VALUE,true)."</pre>";
        if($_RETURN) return $_R; else echo $_R; } 
		
	static public function debugPrint($_VALUE,$_RETURN=false){
        $_R=htmlentities(print_r($_VALUE,true));
        if($_RETURN) return $_R; else echo $_R; }
		
    ## #############################################################################################
    static public function debugStamp(string $_STAMP=null,$_RETURN=null){
        $_DEBUG['time']	=self::debugRequestFloat().' ';
        $_DEBUG['usage']	=self::debugMemoryUsage().'-';
        $_DEBUG['peak']	=self::debugMemoryPeak().'k ';
        $_DEBUG['buffer']='ob'.ob_get_level().' : ';
        $_DEBUG['stamp']	=str_replace([LLTP_ROOT], '', $_STAMP);
        if($_RETURN===true)  return implode($_DEBUG);
        if($_RETURN===false) echo implode($_DEBUG);
        if($_RETURN===null)  $this->__devDebugs[]=implode($_DEBUG); } 
		
	static private function debugRequestFloat(int $_PRECISION=3){
         #return number_format((microtime(true)-$_SERVER['REQUEST_TIME_FLOAT']), $_PRECISION).'s';
         return number_format((microtime(true)-$_SERVER['REQUEST_TIME_FLOAT'])*1000, $_PRECISION).'ms';
    } static private function debugMemoryUsage(int $_PRECISION=1,int $_DIVIDER=1024){
        return number_format(memory_get_usage() / $_DIVIDER, $_PRECISION);
    } static private function debugMemoryPeak(int $_PRECISION=1,int $_DIVIDER=1024){
        return number_format(memory_get_peak_usage() / $_DIVIDER, $_PRECISION);
    }
}
