<?php
/**
 * @TODO unset get debug to turn off debugging
 */
if(($_DEBUG=filter_input(INPUT_GET,'debug'))!==false && $_DEBUG!==null && $_DEBUG!=="0"):
    @call_user_func([$LLTP,'debugStamp'],implode(' ', array_keys(get_defined_vars())));
    @call_user_func([$LLTP,'debugField'],'__devDebugs');
    if($_DEBUG==='debug') @call_user_func([$LLTP,'debugPre'],$LLTP);
    else if(!empty($_DEBUG)) @call_user_func([$LLTP,'debugField'],$_DEBUG);
    endif; unset($_DEBUG);
