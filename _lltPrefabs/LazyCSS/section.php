<h3>Agents</h3>
<pre>
- Set default position of all element to relative.
</pre>

<h3>Colors</h3>
<a style="background-color:var(--fg); color:var(--bg);">Color --bg on --fg</a>
<a style="background-color:var(--bg); color:var(--fg);">Color --fg on --bg</a>

<h3>Outline</h3>
<a style="display:block; font-size:xx-large; filter: var(--outline);">filter: var(--outline);</a>
<a style="display:block; font-size:xx-large; filter: var(--shadow);">filter: var(--shadow);</a>

