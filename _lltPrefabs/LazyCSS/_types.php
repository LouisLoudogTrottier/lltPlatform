<?php
return [
    'LazyCSS'=>[
		'type'=>['','php','html','app'],
		'styles'=>[
			'LazyCSS'=>__DIR__.'/css*.css',
			'flexStretch'=>__DIR__.'/flexStretch.css',
		],
    ],
];