/** jsXHR ########################################################################################
 * XMLHttpRequest (aka ajax) for Lazy Coders
 */
var jsXHR={
    'a':[],
    'b':null,
	send=function(o){
		if(!o.url) return !(console.log('jsXHR Missing o.url'));
		var x = new XMLHttpRequest(); if(!x) return !(console.log('ksXHR x failed'));
		x.onreadystatechange=function(){ 
			if (x.readyState === 4 && x.status!==200) console.log(x.status); 
			if (x.readyState === 4 && o.callback) o.callback(x);
		};
		x.open((o.post?'POST':'GET'), o.url, true);
		if(o.withCredentials && o.user && o.pass) x.setRequestHeader("Authorization", "Basic " + btoa(o.user+":"+o.pass));
		if(o.withCredentials) x.withCredentials = true;
		x.send(jsXHR.d(o.post) || null);  
	},
	d=function(d){
		if(d && d.tagName==='FORM') return new FormData(d); 
		if(d && d.append) return d;
		var r=new FormData(); 
		if (d) for(var k in d) r.append(k, d[k]);
		return r || {};
	},
	json=function(o){
		var callback;
		if(o.callback) callback=o.callback;
		o.callback=function(x){
			if(x && x.responseText) try{ var j=JSON.parse(x.responseText); }catch(e){ console.log(e); }
			if(callback) callback(j||null);
		};
		jsXHR.send(o);
	},
	push=function(o){
		jsXHR.a.push(o);
		if(!jsXHR.b) jsXHR.s();
	},
	s=function(){
		if(!jsXHR.a.length) return (jsXHR.b=null);
		else if(jsXHR.b) return; 
		var o=jsXHR.a.shift(); 
		if(o.callback) var callback = o.callback;
		o.callback=function(x){
			if(x && x.responseText && callback) callback(x);
			else callback();
			jsXHR.n();
		};
		jsXHR.b=true;
		jsXHR.send(o);
	},
	n=function(){
		jsXHR.b=null;
		jsXHR.s();
	}
};




