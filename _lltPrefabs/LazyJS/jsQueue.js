/** jsQueue ####################################################################
 * Create a queue of callback to be ran one after the other with an optional 
 * timeout before the next one runs.
 */

var jsQueue={
	a:[{'t':0}],
	b:null,
	push=function(c,t){    
		jsQueue.a.push({'c':c,'t':t});
		if(!jsQueue.b) jsQueue.s();
	},
	s=function(){
		if(!jsQueue.a.length) return (jsQueue.b=null);
		else if(jsQueue.b) return; 
		else jsQueue.b=true;
		var s=jsQueue.a.shift(); 
		if(s && s.c) (s.c());
		jsQueue.b=setTimeout(jsQueue.n,(s && s.t)?s.t:0);
	},
	n=function(){
		jsQueue.b=null;
		jsQueue.s();
	}
};


/** USAGE ######################################################################
 * 
jsQueue.push(function(){ console.log('Queued0 with 0.5s timeout'); },500);
console.log('NotQueued');
jsQueue.push(function(){ console.log('Queued1 with no timeout'); });
 */

