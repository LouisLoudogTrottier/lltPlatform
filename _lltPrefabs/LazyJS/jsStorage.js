/** jsStorage #####################################################################################
 * Handle local storage by enconding and decoding form to json.
 */
var jsStorage = {
	del=function(k){ window.localStorage.removeItem(k); },
	end=function(){  window.localStorage.clear();       }
};
jsStorage.get=function(k){ 
	var l=window.localStorage.getItem(k);
	if(l) try{ return JSON.parse(l); }catch(e){ console.log(e); } 
};
jsStorage.set=function(k,v){ 
	try{ var i=JSON.stringify(v); } catch(e){ console.log(e); } 
	if(i) window.localStorage.setItem(k,i);
};
