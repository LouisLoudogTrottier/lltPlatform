/** JSREADY ####################################################################
 * push callbacks to be ran once the document is ready,
 */
var jsReady={ 
	a:[],
	push=function(c) { jsReady.a.push(c); jsReady.r(); },
	r=function() {
		if (['interactive', 'complete'].indexOf(document.readyState) > -1)
		while(jsReady.a.length > 0) (jsReady.a.shift()());
	}
};
document.onreadystatechange = jsReady.r;

/** USAGE ######################################################################
 * 
 jsReady.push(function(){
     console.log('document ready');
     document.querySelector('#jsReady').innerHTML='Ready';
 });
 */