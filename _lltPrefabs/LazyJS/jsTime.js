var jsTime = {
	microtime=function(){ return new Date().getTime()/1000; },
	timestamp=function(){ return Math.floor(jsTime.microtime()); },
	date=function(timestamp){
		var d=(timestamp)?new Date(timestamp*1000):new Date();
		if(d) return d.getFullYear()+' '+(d.getMonth()+1)+' '+d.getDate();
	},
	leap=function(year) {
		var d = new Date(year, 1, 28);
		d.setDate(d.getDate() + 1);
		return d.getMonth() === 1;
	},
	age=function(date) {
		var d = new Date(date);
		var n = new Date();
		var y = n.getFullYear() - d.getFullYear();  
		d.setFullYear(d.getFullYear() + y);
		if (d > n){ y--; d.setFullYear(d.getFullYear() - 1); }
		var j=(n.getTime() - d.getTime()) / (3600 * 24 * 1000);
		var a=y+j/(jsTime.leap(n.getFullYear()) ? 366 : 365);
		return Math.floor(a);
	},
	countdown=function(ts){
		var n=jsTime.timestamp();
		var t=ts>n?ts-n:n-ts;
		var d=Math.floor(t/24/60/60); hl=t-(d*24*60*60);
		var h=Math.floor(hl/60/60);   ml=hl-(h*60*60);
		var m=Math.floor(ml/60);
		var s=Math.floor(ml-(m*60));  
		var r='';
		if(d>0) r+=d+'j ';
		if(h>0) r+=h+'h ';
		if(m>0) r+=m+'m ';
		if(s>0) r+=s+'s ';
		return (ts>n) ? r : ('-'+r);       
	}
	
};